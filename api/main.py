from fastapi import FastAPI
from routers import serves, players, returns
app = FastAPI()

app.include_router(serves.router, tags=["Servers"])
app.include_router(players.router, tags=["Players"])
app.include_router(returns.router, tags=["Returns"])
