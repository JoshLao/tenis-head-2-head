steps = [
    [
        # create table
        """
        CREATE TABLE players (
            id SERIAL PRIMARY KEY NOT NULL,
            rank INTEGER NOT NULL,
            first_name VARCHAR(1000) NOT NULL,
            last_name VARCHAR(1000) NOT NULL,
            wins_this_year INTEGER NOT NULL,
            losses_this_year INTEGER NOT NULL,
            serve_id INTEGER NOT NULL,
            return_id INTEGER NOT NULL
        )
        """,
        """
        DROP TABLE players;
        """,
    ]
]
