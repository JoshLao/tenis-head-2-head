steps = [
    [
        """
        CREATE TABLE serves(
            id SERIAL PRIMARY KEY NOT NULL,
            aces INTEGER NOT NULL,
            double_faults INTEGER NOT NULL,
            first_serve_percentage INTEGER NOT NULL,
            first_serve_won INTEGER NOT NULL,
            second_serve_won INTEGER NOT NULL,
            break_points_faced INTEGER NOT NULL,
            break_points_saved INTEGER NOT NULL,
            service_games_played INTEGER NOT NULL,
            service_games_won INTEGER NOT NULL
        )
        """,
        """
        DROP TABLE serve;
        """,
    ]
]
