steps = [
    [
        """
        CREATE TABLE returns(
            id SERIAL PRIMARY KEY NOT NULL,
            return_games_played INTEGER NOT NULL,
            return_games_won INTEGER NOT NULL,
            break_point_opportunities INTEGER NOT NULL,
            break_points_converted INTEGER NOT NULL
        )
        """,
        """
        DROP TABLE returns;
        """,
    ]
]
