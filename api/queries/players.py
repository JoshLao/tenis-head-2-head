import os
from typing import List, Union, Optional
from psycopg_pool import ConnectionPool
from pydantic import BaseModel
from fastapi import HTTPException


pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))



class Error(BaseModel):
    message: str


class PlayersIn(BaseModel):
    rank: int
    first_name: str
    last_name: str
    wins_this_year: int
    losses_this_year: int
    serve_id: int
    return_id: int


class PlayersOut(BaseModel):
    id: int
    rank: int
    first_name: str
    last_name: str
    wins_this_year: int
    losses_this_year: int
    serve_id: int
    return_id: int


class PlayersQueries:
    def get_players(self) -> Union[Error, List[PlayersOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT *
                        FROM players
                        ORDER BY id
                        """,
                    )
                    return [
                        PlayersOut(
                            id=record[0],
                            rank=record[1],
                            first_name=record[2],
                            last_name=record[3],
                            wins_this_year=record[4],
                            losses_this_year=record[5],
                            serve_id=record[6],
                            return_id=record[7],
                        )
                        for record in cur
                    ]
        except Exception as e:
            return {"message": f"could not get a list of players: {e}"}

    def create_players(self, players: PlayersIn) -> PlayersOut:
        try:
             with pool.connection() as conn:
                with conn.cursor() as cur:
                    result = cur.execute(
                        """
                        INSERT INTO players (
                        rank,
                        first_name,
                        last_name,
                        wins_this_year,
                        losses_this_year,
                        serve_id,
                        return_id
                        )
                        VALUES (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id,rank,first_name,last_name,
                        wins_this_year,losses_this_year, serve_id, return_id;

                        """,
                        [
                            players.rank,
                            players.first_name,
                            players.last_name,
                            players.wins_this_year,
                            players.losses_this_year,
                            players.serve_id,
                            players.return_id
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.players_in_to_out(id, players)
        except Exception as e:
            print(e)
            return {"message": f"Could not create player: {e}"}

    def get_one_player(self, id: int) -> Union[PlayersOut,Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT
                            id,
                            rank,
                            first_name,
                            last_name,
                            wins_this_year,
                            losses_this_year,
                            serve_id,
                            return_id
                        FROM players
                        WHERE id = %s;
                        """,
                        [id]
                    )
                    record = cur.fetchone()
                    if record is None:
                        return Error(message="Player not found")
                    return PlayersOut(
                        id=record[0],
                        rank=record[1],
                        first_name=record[2],
                        last_name=record[3],
                        wins_this_year=record[4],
                        losses_this_year=record[5],
                        serve_id=record[6],
                        return_id=record[7]
                    )
        except Exception as e:
            print("THE REAL ERROR ",e)
            return {"message": "Could not find this player"}

    def update_players(self, id: int, players: PlayersIn) -> Union[PlayersOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        UPDATE players
                        SET rank = %s,
                            first_name = %s,
                            last_name = %s,
                            wins_this_year = %s,
                            losses_this_year = %s,
                            serve_id = %s,
                            return_id = %s
                        WHERE id = %s
                        """,
                        [
                            players.rank,
                            players.first_name,
                            players.last_name,
                            players.wins_this_year,
                            players.losses_this_year,
                            players.serve_id,
                            players.return_id,
                            id,
                        ]
                    )
                    return self.players_in_to_out(id, players)
        except Exception:
            return{"message": "Could not update this player"}

    def delete_player(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM players
                        WHERE id = %s
                        """,
                        [id],
                    )
                    return True
        except Exception:
            raise HTTPException(
                status_code=422, detail="Failed to delete"
            )

    def players_in_to_out(self, id: int, players: PlayersIn):
        old_data = players.dict()
        return PlayersOut(id=id, **old_data)
