import os
from typing import List, Union, Optional
from psycopg_pool import ConnectionPool
from pydantic import BaseModel
from fastapi import HTTPException
pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))

class Error(BaseModel):
    message: str

class ReturnIn(BaseModel):
    return_games_played: int
    return_games_won: int
    break_point_opportunities: int
    break_points_converted: int

class ReturnOut(BaseModel):
    id: int
    return_games_played: int
    return_games_won: int
    break_point_opportunities: int
    break_points_converted: int

class ReturnQueries:
    def get_all_returns(self) -> Union[List[ReturnOut],Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT * FROM returns
                        ORDER BY id
                        """
                    )
                    records = cur.fetchall()
                    returns = [
                        ReturnOut(
                            id=record[0],
                            return_games_played=record[1],
                            return_games_won=record[2],
                            break_point_opportunities=record[3],
                            break_points_converted=record[4]
                        )
                        for record in records ]
                    if returns:
                        return returns
                    else:
                        return Error(message="could not return all returns")
        except Exception as e:
            print(e)
            return {'message': 'Could not get all return stats'}

    def get_one_return(self, return_id: int) -> Union[ReturnOut,Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT * FROM returns
                        WHERE id=%s
                        """,
                        [return_id]
                    )
                    record = cur.fetchone()
                    if record:
                        return ReturnOut(
                            id=record[0],
                            return_games_played=record[1],
                            return_games_won=record[2],
                            break_point_opportunities=record[3],
                            break_points_converted=record[4]
                        )
                    else:
                        return Error(message="Return stats record not found")
        except Exception as e:
            print(e)
            return {'message': 'Could not get return stats'}

    def create_return(self, ret: ReturnIn) -> Union[ReturnOut,Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        INSERT INTO returns(
                            return_games_played,
                            return_games_won,
                            break_point_opportunities,
                            break_points_converted
                        )
                        VALUES(%s,%s,%s,%s)
                        RETURNING id,
                        return_games_played,
                        return_games_won,
                        break_point_opportunities,
                        break_points_converted
                        """,
                        [
                            ret.return_games_played,
                            ret.return_games_won,
                            ret.break_point_opportunities,
                            ret.break_points_converted
                        ]
                    )
                    record = cur.fetchone()
                    print(record)
                    if record:
                        return ReturnOut(
                            id=record[0],
                            return_games_played=record[1],
                            return_games_won=record[2],
                            break_point_opportunities=record[3],
                            break_points_converted=record[4]
                        )
                    else:
                        return Error(message="Return stats record not found")
        except Exception as e:
            return {'message': f'Could not create return stats: {e}'}

    def update_return(self, return_id: int, ret: ReturnIn) -> Union[ReturnOut,Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        UPDATE returns
                        SET
                        return_games_played=%s,
                        return_games_won=%s,
                        break_point_opportunities=%s,
                        break_points_converted=%s
                        WHERE id=%s
                        RETURNING id,
                        return_games_played,
                        return_games_won,
                        break_point_opportunities,
                        break_points_converted
                        """,
                        [
                            ret.return_games_played,
                            ret.return_games_won,
                            ret.break_point_opportunities,
                            ret.break_points_converted,
                            return_id
                        ]
                    )
                    record = cur.fetchone()
                    print(record)
                    if record:
                        return ReturnOut(
                            id=record[0],
                            return_games_played=record[1],
                            return_games_won=record[2],
                            break_point_opportunities=record[3],
                            break_points_converted=record[4]
                        )
                    else:
                        return Error(message="Return stats record not found")
        except Exception as e:
            print(e)
            return {'message': f'Could not create return stats: {e}'}

    def delete_return(self, return_id: int) -> Union[str, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM returns
                        WHERE id=%s;
                        """,
                        [return_id]
                    )
                    return True
        except Exception as e:
            return Error(message=f"Could not delete serve record: {e}")
