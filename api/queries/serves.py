import os
from typing import List, Union
from psycopg_pool import ConnectionPool
from pydantic import BaseModel

pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))


class Error(BaseModel):
    message: str


class ServeIn(BaseModel):
    aces: int
    double_faults: int
    first_serve_percentage: int
    first_serve_won: int
    second_serve_won: int
    break_points_faced: int
    break_points_saved: int
    service_games_played: int
    service_games_won: int


class ServeOut(BaseModel):
    id: int
    aces: int
    double_faults: int
    first_serve_percentage: int
    first_serve_won: int
    second_serve_won: int
    break_points_faced: int
    break_points_saved: int
    service_games_played: int
    service_games_won: int


class ServeQueries:

    def get_all_serves(self) -> List[ServeOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT *
                        FROM serves
                        ORDER BY id;
                        """,
                    )
                    records = cur.fetchall()
                    serves = []
                    for record in records:
                        s_data = {
                            "id": record[0],
                            "aces": record[1],
                            "double_faults": record[2],
                            "first_serve_percentage": record[3],
                            "first_serve_won": record[4],
                            "second_serve_won": record[5],
                            "break_points_faced": record[6],
                            "break_points_saved": record[7],
                            "service_games_played": record[8],
                            "service_games_won": record[9],
                        }
                        serves.append(ServeOut(**s_data))
                    return serves
        except Exception as e:
            print("Error: ", e)
            return {"message": "Could not retrieve all serve stats"}


    def create_serve(self, serve: ServeIn) -> Union[ServeOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        INSERT INTO serves (aces, double_faults, first_serve_percentage,
                        first_serve_won, second_serve_won, break_points_faced, break_points_saved,
                        service_games_played, service_games_won)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING id, aces, double_faults, first_serve_percentage, first_serve_won,
                        second_serve_won, break_points_faced,
                        break_points_saved, service_games_played, service_games_won;
                        """,
                        (
                            serve.aces,
                            serve.double_faults,
                            serve.first_serve_percentage,
                            serve.first_serve_won,
                            serve.second_serve_won,
                            serve.break_points_faced,
                            serve.break_points_saved,
                            serve.service_games_played,
                            serve.service_games_won,
                        ),
                    )
                    record = cur.fetchone()
                    return ServeOut(
                        id=record[0],
                        aces=record[1],
                        double_faults=record[2],
                        first_serve_percentage=record[3],
                        first_serve_won=record[4],
                        second_serve_wbreak_points_savedon=record[5],
                        break_points_faced=record[6],
                        break_points_saved=record[7],
                        service_games_played=record[8],
                        service_games_won=record[9],
                    )
        except Exception as e:
            print("Error: ", e)
            return {"message": "Could not create serve record"}


    def get_serve_by_id(self, serve_id: int) -> Union[ServeOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT id, aces, double_faults, first_serve_percentage,
                        first_serve_won, second_serve_won, break_points_faced,
                        break_points_saved, service_games_played, service_games_won
                        FROM serves
                        WHERE id = %s;
                        """,
                        (serve_id),
                    )
                    record = cur.fetchone()
                    if record:
                        return ServeOut(
                            id=record[0],
                            aces=record[1],
                            double_faults=record[2],
                            first_serve_percentage=record[3],
                            first_serve_won=record[4],
                            second_serve_won=record[5],
                            break_points_faced=record[6],
                            break_points_saved=record[7],
                            service_games_played=record[8],
                            service_games_won=record[9],
                        )
                    else:
                        return Error(message="Serve record not found")
        except Exception as e:
            return {"message": "Could not get serve stats {e}"}


    def update_serve(self, serve_id: int, serve: ServeIn) -> Union[ServeOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        UPDATE serves
                        SET
                        aces = %s,
                        double_faults = %s,
                        first_serve_percentage = %s,
                        first_serve_won = %s,
                        second_serve_won = %s,
                        break_points_faced = %s,
                        break_points_saved = %s,
                        service_games_played = %s,
                        service_games_won = %s
                        WHERE id = %s
                        RETURNING id, aces, double_faults, first_serve_percentage,
                        first_serve_won, second_serve_won, break_points_faced,
                        break_points_saved, service_games_played, service_games_won;
                        """,
                        (
                            serve.aces,
                            serve.double_faults,
                            serve.first_serve_percentage,
                            serve.first_serve_won,
                            serve.second_serve_won,
                            serve.break_points_faced,
                            serve.break_points_saved,
                            serve.service_games_played,
                            serve.service_games_won,
                            serve_id,
                        ),
                    )
                    record = cur.fetchone()
                    if record:
                        return ServeOut(
                            id=record[0],
                            aces=record[1],
                            double_faults=record[2],
                            first_serve_percentage=record[3],
                            first_serve_won=record[4],
                            second_serve_won=record[5],
                            break_points_faced=record[6],
                            break_points_saved=record[7],
                            service_games_played=record[8],
                            service_games_won=record[9],
                        )
                    else:
                        return Error(message="Serve record not found")
        except Exception as e:
            return {"message": "Could not get serve stats {e}"}


    def delete_serve(self, serve_id: int) -> Union[str, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        DELETE FROM serves
                        WHERE id = %s;
                        """,
                        [serve_id],
                    )
                    return True
        except Exception as e:
            return Error(message=f"Could not delete serve record: {e}")
