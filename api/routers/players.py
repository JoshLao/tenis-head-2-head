from fastapi import (APIRouter, Depends,)
from queries.players import (PlayersIn, PlayersOut, PlayersQueries, Error,)
from typing import Union, List, Optional

router = APIRouter()

@router.get("/api/player", response_model=Union[List[PlayersOut], Error])
def get_players(
    queries: PlayersQueries = Depends(),
):
    return queries.get_players()

@router.post("/api/player", response_model=Union[PlayersOut, Error])
def create_players(
    players: PlayersIn,
    queries: PlayersQueries = Depends(),
):
    return queries.create_players(players)

@router.get("/api/player/{player_id}", response_model=Union[PlayersOut,Error])
def get_one_player(
    id: int,
    queries: PlayersQueries = Depends()
) -> Union[PlayersOut,Error]:
    return queries.get_one_player(id)

@router.put("/api/players/{player_id}", response_model=Union[
    PlayersOut, Error])
def update_players(
    id: int,
    player: PlayersIn,
    queries: PlayersQueries = Depends(),
) -> Union[PlayersOut, Error]:
    return queries.update_players(id, player)

@router.delete("/api/player/{player_id}", response_model=bool)
def delete_player(
    players_id: int,
    queries: PlayersQueries = Depends(),
) -> bool:
    return queries.delete_player(players_id)
