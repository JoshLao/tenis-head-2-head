from fastapi import APIRouter, Depends, Response
from typing import Union, List
from queries.returns import ReturnIn, ReturnOut, Error, ReturnQueries

router = APIRouter()

@router.get("/api/returns", response_model=Union[List[ReturnOut],Error])
def get_all(
    repo: ReturnQueries = Depends()
):
    return repo.get_all_returns()

@router.get("/api/returns/{id}", response_model=Union[ReturnOut,Error])
def get_one(
    return_id: int,
    response: Response,
    repo: ReturnQueries = Depends(),
):
    ret = repo.get_one_return(return_id)
    if ret is None:
        response.status_code=404
    return ret


@router.post("/api/returns", response_model=Union[ReturnOut,Error])
def create(
    ret: ReturnIn,
    response: Response,
    repo: ReturnQueries = Depends()
):
    new_return = repo.create_return(ret)
    if new_return is None:
        response.status_code = 400
    return new_return

@router.put("/api/returns/{id}", response_model=Union[ReturnOut,Error])
def update(
    return_id: int,
    ret: ReturnIn,
    response: Response,
    repo: ReturnQueries = Depends()
):
    updated_return = repo.update_return(return_id,ret)
    if updated_return is None:
        response.status_code = 400
    return updated_return

@router.delete("/api/returns/{id}", response_model=bool)
def delete(
    return_id: int,
    repo: ReturnQueries = Depends()
):
        return repo.delete_return(return_id)