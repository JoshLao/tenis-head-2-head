from fastapi import APIRouter, Depends, Response
from typing import Union, List
from queries.serves import (
    ServeIn,
    ServeOut,
    Error,
    ServeQueries
)
router = APIRouter()

@router.get("/api/serves", response_model=List[ServeOut])
def get_all(
    repo: ServeQueries = Depends(),
):
    return repo.get_all_serves()

@router.get("/api/serves/{id}", response_model=Union[ServeOut,Error])
def get_one_serve(
    serve_id: int,
    response: Response,
    repo: ServeQueries = Depends(),
):
    serve = repo.get_serve_by_id(serve_id)
    if serve is None:
        response.status_code = 404
    return serve

@router.post("/api/serves", response_model=Union[ServeOut,Error])
def create_serve(
    serve: ServeIn,
    response:   Response,
    repo: ServeQueries = Depends(),
):
    new_serve = repo.create_serve(serve)
    if new_serve is None:
        response.status_code = 400
    return new_serve

@router.put("/api/serves/{id}", response_model=Union[ServeOut,Error])
def update_serve(
    serve_id: int,
    serve: ServeIn,
    repo: ServeQueries = Depends(),
):
    return repo.update_serve(serve_id,serve)

@router.delete("/api/serves/{id}", response_model=bool)
def delete_serve(
    serve_id: int,
    repo: ServeQueries = Depends(),
):
    return repo.delete_serve(serve_id)